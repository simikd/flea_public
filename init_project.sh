#!/ubin/bash

cd docker
./docker-rebuild
cd ../

npm install
composer update
composer install

cd public && ln -s ../storage/app storage

cd ../docker && docker exec -it flea-web chown www-data:www-data -R ./


