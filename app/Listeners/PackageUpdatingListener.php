<?php


namespace App\Listeners;


use App\Events\PackageUpdatingEvent;
use App\Factories\PackageUpdaterChecksFactory;

class PackageUpdatingListener
{

	public const NEEDS_TO_BE_CHECKED = [
		'status', 'payment', 'shipping'
	];

	public function handle(PackageUpdatingEvent $packageUpdatingEvent): bool
	{
		$package = $packageUpdatingEvent->getPackage();
		$attributesChanged = $package->getDirty();
		foreach ($attributesChanged as $name => $value) {
			if (!in_array($name, self::NEEDS_TO_BE_CHECKED)) {
				continue;
			}

			PackageUpdaterChecksFactory::build($name)->checkBeforeUpdate($package);
		}

		return true;
	}

}
