<?php


namespace App\Http\Controllers;


use App\Models\Market;
use Illuminate\Http\Response;

class MarketController extends Controller
{

	public function listMarkets () : Response
	{
		$markets = Market::with(['categories' => static function ($query) {
			$query->where('level', '=', 1);
			$query->with('subCategories');
		}])->get();
		if ($markets) {
			return \response(json_encode($markets), Response::HTTP_OK);
		}

		return \response(json_encode([]), Response::HTTP_NOT_FOUND);
	}

}
