<?php


namespace App\Http\Controllers;


use App\Services\UserService;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{

	public function changeUserProfile(Request $request, UserService $userService): Response
	{
		$user = $userService->getUser();
		$name = $request->input('name');
		$value = $request->input('value');

		if (!$name || !$value) {
			throw new RuntimeException('Empty data in change user profile action.');
		}

		$user->$name = $value;
		$user->save();

		return \response(json_encode($user), Response::HTTP_OK);
	}

	public function getUserData(UserService $userService): Response
	{
		$user = $userService->getUser();

		return \response(json_encode($user), Response::HTTP_OK);
	}

}
