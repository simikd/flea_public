<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 23.12.2018
 * Time: 20:10
 */

namespace App\Http\Controllers;


use App\Services\ImageService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{

	public function uploadPictures(Request $request, ImageService $imageService, UserService $userService)
	{
		$files = $request->file('pictures');
		$uploadedFiles = [];

		if ($request->hasFile('pictures')) {
			$uploadedFiles = $imageService->uploadImages($files, $userService->getUser()->id);
		}

		return response(json_encode($uploadedFiles), Response::HTTP_OK);

	}

	public function deletePicture(String $filename, UserService $userService): void
	{
		$hash = hash('sha256', $userService->getUser()->id);
		$path = 'temp/' . $hash . '/';
		Storage::delete($path . $filename);
	}

	public function deleteAllPictures(UserService $userService)
	{
		$hash = hash('sha256', $userService->getUser()->id);
		$path = 'temp/' . $hash;

		$result = Storage::deleteDirectory($path);

		return response()->json($result);
	}


}
