<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.1.2019
 * Time: 21:51
 */

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\Item;

class HomepageController extends Controller
{

	public function homepageAction()
	{
		$categories = Category::where('parent_id', Category::MAIN_CATEGORIES_PARENT_ID)->get();

		return view('homepage.homepage', [
			'categories' => $categories,
			'items' => Item::with('uploads')->with('seller')->get()
		]);
	}

}
