<?php


namespace App\Http\Controllers;


use App\Models\City;
use Illuminate\Http\Response;

class CityController extends Controller
{

	public function listCities(): Response
	{
		return response(json_encode(City::get()), Response::HTTP_OK);
	}

}
