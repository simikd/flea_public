<?php


namespace App\Http\Controllers;


use App\Http\Validators\CreateOrderValidator;
use App\Models\CartItem;
use App\Models\Package;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class OrderController extends Controller
{

	private $orderService;

	public function __construct(OrderService $orderService)
	{
		$this->orderService = $orderService;
	}

	public function addItem(Request $request, UserService $userService): View
	{
		$this->orderService->addItem($request->input('item_id'), $userService->getUser());

		return view('components.includes.shopping_cart_items', ['packages' => $this->orderService->getUserPackagesData($userService->getUser()->id, false)]);
	}

	public function removeItem(Request $request): Response
	{
		$cartItem = CartItem::find($request->input('cart_item_id'));
		$package = Package::find($cartItem->package_id);
		$cartItem->delete();

		if ($package->items->isEmpty()) {
			$package->delete();
		}

		return \response('Item deleted.', Response::HTTP_OK);
	}

	public function listPackages(bool $checked, UserService $userService): Response
	{
		$user = $userService->getUser();

		return \response(json_encode($this->orderService->getUserPackagesData($user->id, $checked)), Response::HTTP_OK);
	}

	public function updatePackage(Request $request): Response
	{
		$setAllToIdleFirst = $request->input('all_idle');
		$package = Package::find($request->input('package_id'));

		if ($setAllToIdleFirst) {
			Package::where(['user_id' => $package->user_id, 'status' => Package::STATUS_CHECKED])->where('id', '!=', $package->id)->update(['status' => 'idle']);
		}

		$package = $this->orderService->updatePackage($package, json_decode($request->input('update_data'), true));

		return \response(json_encode($package), Response::HTTP_OK);
	}

	public function updatePackages(Request $request): Response
	{
		$packageIds = explode(', ', $request->input('packages'));
		$packages = [];

		foreach ($packageIds as $packageId) {
			$package = Package::find($packageId);
			$packages[] = $this->orderService->updatePackage($package, json_decode($request->input('update_data'), true));

		}

		return \response(json_encode($packages), Response::HTTP_OK);
	}

	public function createOrder(Request $request, UserService $userService, OrderService $orderService): Response
	{
		$errors = CreateOrderValidator::validate($request);
		if ($errors) {
			return \response(json_encode(['errors' => $errors]), Response::HTTP_OK);
		}

		$user = $userService->updateUser(json_decode($request->input('user'), true), (bool)json_decode($request->input('register'), true));
		$order = $this->orderService->createOrder($user, explode(', ', $request->input('packages')));

		return \response(json_encode($orderService->getOrderData($order->id)), Response::HTTP_OK);
	}

}
