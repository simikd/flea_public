<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 26.1.2019
 * Time: 14:24
 */

namespace App\Http\Controllers;


use App\Http\Validators\CreateItemValidator;
use App\Models\Category;
use App\Models\Item;
use App\Models\User;
use App\Models\UserReview;
use App\Services\ImageService;
use App\Services\ItemService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ItemController extends Controller
{

	public function showItemForm(): View
	{
		return view('item.create');
	}

	public function showItem(String $slug): View
	{
		/* @var Item $item */
		$item = Item::where('slug', $slug)->with('uploads')->first();
		if (!isset($item->id)) {
			abort(404);
		}

		return view('item.show',
			[
				'item' => $item,
				'user' => User::find($item->user_id),
				'statusMessage' => $item->getStatusMessage(),
				'userReviews' => UserReview::where('user_id', $item->user_id)->get(),
				'userItems' => Item::where('user_id', $item->user_id)->where('id', '!=', $item->id)->get(),
				'categories' => Category::where('parent_id', Category::find($item->category_id)->parent_id)->get()
			]
		);
	}

	public function createItem(Request $request, UserService $userService, ImageService $imageService, ItemService $itemService): Response
	{
		$errors = CreateItemValidator::validate($request);
		if ($errors) {
			return \response(json_encode(['errors' => $errors]), Response::HTTP_OK);
		}

		$user = $userService->updateUser(json_decode($request->input('user'), true), false);

		$itemData = json_decode($request->input('item'), true);
		$itemData['user_id'] = $user->id;
		$item = $itemService->createItem($itemData);

		$uploads = $request->input('uploads');
		if (!empty($uploads)) {
			$imageService->saveItemPictures(json_decode($uploads, true), $user, $item);
		}

		return response(json_encode($item), Response::HTTP_OK);
	}

}
