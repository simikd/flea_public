<?php


namespace App\Http\Controllers;


use App\Models\Category;
use App\Services\BreadcrumbsService;
use Illuminate\Http\Response;

class BreadcrumbsController extends Controller
{

	public function categories(int $categoryId, BreadcrumbsService $breadcrumbsService): Response
	{
		$category = Category::with('subCategories')->find($categoryId);

		if (!$category) {
			return response(json_encode([]), Response::HTTP_NOT_FOUND);
		}

		return response(json_encode($breadcrumbsService->getBreadcrumbs($category)), Response::HTTP_OK);
	}

}
