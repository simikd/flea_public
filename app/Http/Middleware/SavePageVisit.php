<?php

namespace App\Http\Middleware;

use App\Models\PageVisit;
use App\Services\UserService;
use Closure;

class SavePageVisit
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->userService->getUser();
        $pageVisit = new PageVisit;
        $pageVisit->user_id = $user->id;
        $pageVisit->slug = $request->get('slug') ?? $request->path();
        $pageVisit->referer = $request->server('HTTP_REFERER');
        $pageVisit->save();

        return $next($request);
    }

}
