<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Services\UserService;
use Closure;

class CheckUser
{

	private $userService;

	public function __construct(UserService $userService)
	{
		$this->userService = $userService;
	}

	public function handle($request, Closure $next)
	{
		$user = User::find($request->cookie('userId'));
		if (isset($user->id)) {
			$this->userService->initializeUserSession($user);

			return $next($request);
		}

		$this->userService->checkAndInitializeUnknownUserByIp($request->ip());

		return $next($request);
	}
}
