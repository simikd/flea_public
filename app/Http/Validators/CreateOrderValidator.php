<?php


namespace App\Http\Validators;


use App\Interfaces\CustomValidationInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateOrderValidator implements CustomValidationInterface
{

	public const ITEM_FORM_USER_VALIDATION = [
		'name' => 'required|max:64',
		'surname' => 'max:64',
		'email' => 'email|required|max:64',
		'city' => 'required|max:64',
		'street' => 'required|max:128',
		'street_number' => 'required|max:16',
		'zip_code' => 'required|max:5'
	];

	public static function validate(Request $request): array
	{
		$errors = [];
		$validationUserErrors = Validator::make(json_decode($request->input('user'), true), self::ITEM_FORM_USER_VALIDATION);
		if  (!empty($validationUserErrors->errors()->all())) {
			$errors[] = 'Skontrolujte prosím, či sú správne vyplnené všetky polia';
		}

		return $errors;
	}
}