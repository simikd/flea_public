<?php


namespace App\Http\Validators;


use App\Interfaces\CustomValidationInterface;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateItemValidator implements CustomValidationInterface
{

	public const ITEM_FORM_ITEM_VALIDATION = [
		'title' => 'required|max:64',
		'description' => 'required',
		'price' => 'numeric|between:0,9999999.99',
		'price_from' => 'numeric|between:0,9999999.99',
		'price_to' => 'numeric|between:0,9999999.99',
		'shipping_mail' => 'required_without:shipping_in_person',
		'shipping_in_person' => 'required_without:shipping_mail',
		'type' => 'in:buy,sell,give',
		'city_id' => 'required_if:shipping_in_person,==,true'
	];

	public const ITEM_FORM_USER_VALIDATION = [
		'name' => 'required|max:64',
		'email' => 'required|max:64',
		'phone' => 'required_if:shipping_in_person,==,true|max:64|regex:/^\+?4?2?[0-9]+/|nullable'
	];

	public static function validate(Request $request): array
	{
		$userData = json_decode($request->input('user'), true);
		$itemData = json_decode($request->input('item'), true);
		$terms = json_decode($request->input('terms'), true);
		$errors = [];

		if (!$terms) {
			$errors[] = 'Pre pridanie inzerátu je potrebné súhlasiť s pravidlami portálu flea.sk';
		}

		if ( isset($itemData['city_id']) && $itemData['city_id'] && !City::find($itemData['city_id']) ) {
			$errors[] = 'Zadali ste neexistujújce mesto, vyberte prosím mesto zo zoznamu.';
		}

		if (!isset($itemData['category_id']) || !$itemData['category_id']) {
			$errors[] = 'Vyplňte prosím kategóriu pre váš inzerát';
		}

		$validationItemErrors = Validator::make($itemData, self::ITEM_FORM_ITEM_VALIDATION);
		$validationUserErrors = Validator::make($userData, self::ITEM_FORM_USER_VALIDATION);
		if  (!empty($validationItemErrors->errors()->all()) || !empty($validationUserErrors->errors()->all())) {
			$errors[] = 'Skontrolujte prosím, či sú správne vyplnené všetky polia';
		}

		return $errors;
	}

}