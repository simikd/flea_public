<?php

namespace App\Providers;

use App\Services\BreadcrumbsService;
use App\Services\ImageService;
use App\Services\ItemService;
use App\Services\OrderService;
use App\Services\UserService;
use App\Services\UserTrackingService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(UserTrackingService::class, function() {
			return new UserTrackingService();
		});
		$this->app->singleton(OrderService::class, function () {
			return new OrderService();
		});
		$this->app->singleton(UserService::class, function () {
			return new UserService($this->app->make(UserTrackingService::class), $this->app->make(OrderService::class));
		});
		$this->app->singleton(BreadcrumbsService::class, function () {
			return new BreadcrumbsService();
		});
		$this->app->singleton(ImageService::class, function () {
			return new ImageService();
		});
		$this->app->singleton(ItemService::class, function() {
			return new ItemService();
		});
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}
}
