<?php


namespace App\Models\ModelChecks;


use App\Interfaces\PackageUpdaterChecksInterface;
use App\Models\Package;
use App\Services\OrderService;
use RuntimeException;

class PackageStatusUpdaterCheck implements PackageUpdaterChecksInterface
{
	public const ALLOWED_STATUS_CHANGES = [
		'' => [Package::STATUS_IDLE],
		Package::STATUS_IDLE => [Package::STATUS_CHECKED],
		Package::STATUS_CHECKED => [Package::STATUS_CHECKED, Package::STATUS_IDLE],
		Package::STATUS_DELETED => [],
		Package::STATUS_ORDERED => [Package::STATUS_CANCELLED, Package::STATUS_DELIVERED],
		Package::STATUS_DELIVERED => [Package::STATUS_RETURNED],
		Package::STATUS_RETURNED => []
	];

	public function checkBeforeUpdate(Package $package): bool
	{
		$oldValue = $package->getOriginal('status');
		if (!in_array($package->status, self::ALLOWED_STATUS_CHANGES[$oldValue])) {
			throw new RuntimeException('Forbidden status change for package id: ' . $package->id . ' old status: ' . $oldValue . ' new status: ' . $package->status);
		}

		return true;
	}
}
