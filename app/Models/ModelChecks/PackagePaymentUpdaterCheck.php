<?php


namespace App\Models\ModelChecks;


use App\Interfaces\PackageUpdaterChecksInterface;
use App\Models\Package;
use App\Services\OrderService;
use RuntimeException;

class PackagePaymentUpdaterCheck implements PackageUpdaterChecksInterface
{

	public function checkBeforeUpdate(Package $package): bool
	{
		if ($package->status !== Package::STATUS_CHECKED) {
			throw new RuntimeException('Forbidden payment change for package id: ' . $package->id . ' with status: ' . $package->status);
		}

		return true;
	}
}
