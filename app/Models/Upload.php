<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 24.12.2018
 * Time: 13:46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Upload extends Model
{

    protected $fillable = ['filename', 'mime', 'path', 'size', 'thumb_filename', 'width', 'height', 'horizontal', 'item_id'];

    public function item(): BelongsTo
    {
        return $this->belongsTo(Item::class);
    }

    public function getUrl(bool $isThumb = false): String
    {
        $path = 'storage/' . $this->path;

        return $isThumb ? $path . $this->thumb_filename : $path . $this->filename;
    }

}
