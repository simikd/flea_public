<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Market extends Model
{
    public function Categories(): HasMany
    {
        return $this->hasMany(Category::class);
    }
}
