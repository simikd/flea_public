<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'item_id',
		'package_id'
	];

	protected $attributes = [
		'count' => 1
	];

	public function package(): belongsTo
	{
		return $this->belongsTo(Package::class);
	}

}
