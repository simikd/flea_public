<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 22.12.2018
 * Time: 20:11
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{

	public const MAIN_CATEGORIES_PARENT_ID = 0;

   public function market(): BelongsTo
   {
       return $this->belongsTo(Market::class);
   }

	public function categories(): HasMany
	{
		return $this->hasMany(__CLASS__, 'parent_id', 'id');
	}

	public function subCategories()
	{
		return $this->categories()->with('subCategories');
	}
}
