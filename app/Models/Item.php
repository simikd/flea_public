<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Item extends Model
{

	public const STATUS_AVAILABLE = 'available';
	public const STATUS_IN_CART = 'in_cart';
	public const STATUS_ORDERED = 'ordered';

	public const ITEM_AVAILABLE_PACKAGE_STATUSES = [
		Package::STATUS_DELETED, Package::STATUS_CANCELLED, Package::STATUS_RETURNED
	];

	public const ITEM_ORDERED_PACKAGE_STATUSES = [
		Package::STATUS_ORDERED, Package::STATUS_DELIVERED
	];


	public const ITEM_STATUS_MESSAGES = [
		self::STATUS_AVAILABLE => '',
		self::STATUS_IN_CART => 'Predmet bol pridaný do košíka.',
		self::STATUS_ORDERED => 'Predmet bol objednaný užívateľom. V prípade zrušenia objednávky predmet opäť sprístupníme pre objednanie.'
	];

	protected $fillable = ['title', 'description', 'price', 'price_from', 'price_to', 'category_id', 'user_id', 'shipping_mail', 'shipping_in_person',
		'payment_on_arrival', 'payment_transfer', 'type', 'planned_on', 'shipping_price', 'city', 'bargain'];

	public function uploads(): HasMany
	{
		return $this->hasMany(Upload::class);
	}

	public function seller(): HasOne
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	private function getStatus(): string
	{
		$package = Package::where(['seller_id' => $this->user_id, 'user_id' => $this->id])->get()->first();
		if (!$package || !$package->items->contains('item_id', $this->id) || in_array($package->status, self::ITEM_AVAILABLE_PACKAGE_STATUSES, true)) {
			return self::STATUS_AVAILABLE;
		}

		if (in_array($package->status, self::ITEM_ORDERED_PACKAGE_STATUSES, true)) {
			return self::STATUS_ORDERED;
		}

		return self::STATUS_IN_CART;
	}

	public function getStatusMessage(): string
	{
		return self::ITEM_STATUS_MESSAGES[$this->getStatus()];
	}

}
