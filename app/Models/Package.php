<?php


namespace App\Models;


use App\Events\PackageUpdatingEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
	use SoftDeletes;

	public const STATUS_IDLE = 'idle';
	public const STATUS_CHECKED = 'checked';
	public const STATUS_DELETED = 'deleted';
	public const STATUS_ORDERED = 'ordered';
	public const STATUS_DELIVERED = 'delivered';
	public const STATUS_CANCELLED = 'cancelled';
	public const STATUS_RETURNED = 'returned';


	protected $fillable = ['user_id', 'seller_id'];

	protected $dispatchesEvents = [
		'updating' => PackageUpdatingEvent::class,
	];

	public function items(): hasMany
	{
		return $this->hasMany(CartItem::class);
	}

}
