<?php


namespace App\Factories;


use App\Interfaces\PackageUpdaterChecksInterface;
use RuntimeException;

class PackageUpdaterChecksFactory
{

	public const PACKAGE_UPDATER_CHECKS_PATH = 'App\Models\ModelChecks\\';

	public static function build(string $attributeName): PackageUpdaterChecksInterface
	{
		$className = str_replace('_', '', ucwords($attributeName, '_'));
		$attributeClass = self::PACKAGE_UPDATER_CHECKS_PATH . 'Package' . $className . 'UpdaterCheck';
		if (class_exists($attributeClass)) {
			return new $attributeClass;
		}

		throw new RuntimeException('Trying to instantiate nonexistent class ' . $attributeClass . '.');
	}

}
