<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 6.2.2019
 * Time: 17:27
 */

namespace App\Services;


use App\Models\Category;
use App\Models\Item;
use App\Models\Upload;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageService
{

	public const THUMB_HEIGHT = 320;
	public const IMAGE_EXTENSIONS_ALLOWED = ['jpg', 'jpeg', 'png', 'gif'];

	public function uploadImages($files, int $userId): array
	{
		$hash = hash('sha256', $userId);
		$path = 'temp/' . $hash . '/';
		$uploadedFiles = [];

		foreach ($files as $file) {
			$ext = $file->getClientOriginalExtension();

			if (!in_array($ext, self::IMAGE_EXTENSIONS_ALLOWED, true)) {
				continue;
			}

			$filename = time() . '_' . str_replace(' ', '_', $file->getClientOriginalName());

			if (Storage::putFileAs($path, $file, $filename)) {
				$uploadedFiles[] = [
					'path' => 'storage/' . $path . $filename,
					'filename' => $filename
				];
			}
		}

		return $uploadedFiles;
	}

	public function saveItemPictures(array $uploads, User $user, Item $item): void
	{
		$userHash = hash('sha256', $user->id);
		$newPath = 'public/' . $userHash . '/';
		$i = 1;
		foreach ($uploads as $fileData) {
			$file = str_replace('storage', '', $fileData['path']);
			$size = Storage::size($file);
			$mime = Storage::mimeType($file);
			$exploded = explode('.', $file);
			$ext = end($exploded);
			$category = Category::find($item->category_id);
			$filename = $category->name . '_' . $item->title . '_' . $i . time() . '.' . $ext;

			$image = Image::make($fileData['path']);
			if ($image->width() > 2000) {
				$image = $image->resize(2000, null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$image->save();
			}
			Storage::move($file, $newPath . $filename);
			$this->createThumbnail($image, $newPath . 'thumb-' . $filename, self::THUMB_HEIGHT);

			$originalWidth = $image->width();
			$originalHeight = $image->height();

			Upload::create([
				'filename' => $filename,
				'mime' => $mime,
				'size' => $size,
				'path' => $newPath,
				'thumb_filename' => 'thumb-' . $filename,
				'width' => $originalWidth,
				'height' => $originalHeight,
				'horizontal' => $originalWidth > $originalHeight,
				'item_id' => $item->id,
			]);

			$i++;
		}
	}

	private function createThumbnail($image, $newPath, $height): void
	{
		$image = $image->resize(null, $height, function ($constraint) {
			$constraint->aspectRatio();
		});
		$image->save(storage_path('app/' . $newPath));
	}


}
