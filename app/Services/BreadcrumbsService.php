<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 5.2.2019
 * Time: 10:17
 */

namespace App\Services;


use App\Models\Category;
use App\Models\Market;

class BreadcrumbsService
{

	public function getBreadcrumbs(Category $category): array
	{
		$breadcrumbs = [];
		$breadcrumbs[] = Market::with(['categories' => static function ($query) {
			$query->where('level', '=', 1);
			$query->with('subCategories');
		}])->find($category->market_id);
		if ($category->parent_id) {
			$breadcrumbs = $this->getParentBreadcrumb($category->parent_id, $breadcrumbs);
		}
		$breadcrumbs[] = $category;

		return $breadcrumbs;
	}

	private function getParentBreadcrumb(int $parentId, array $breadcrumbs): array
	{
		$parentCategory = Category::with('subCategories')->find($parentId);
		if ($parentCategory->parent_id !== 0) {
			$breadcrumbs = $this->getParentBreadcrumb($parentCategory->parent_id, $breadcrumbs);
		}

		$breadcrumbs[] = $parentCategory;

		return $breadcrumbs;
	}

}
