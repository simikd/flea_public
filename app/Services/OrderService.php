<?php


namespace App\Services;


use App\Models\CartItem;
use App\Models\Item;
use App\Models\Order;
use App\Models\Package;
use App\Models\Upload;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrderService
{


	public function getUserPackagesData(int $userId, bool $checked): array
	{
		$getStatusesArray = [Package::STATUS_CHECKED];
		if (!$checked) {
			$getStatusesArray[] = Package::STATUS_IDLE;
		}

		return $this->preparePackageResultForResponse(
			$this->getPackageItemsQuery()
				->where('packages.user_id', $userId)
				->whereIn('packages.status', $getStatusesArray)
				->get()
		);
	}

	public function addItem(int $itemId, User $user): void
	{
		$item = Item::find($itemId);
		$package = Package::where(['user_id' => $user->id, 'seller_id' => $item->user_id])->whereIn('status', [Package::STATUS_IDLE, Package::STATUS_CHECKED])->first();

		if (!$package) {
			$package = Package::create(['user_id' => $user->id, 'seller_id' => $item->user_id, 'status' => Package::STATUS_IDLE]);
		}

		$cartItem = CartItem::where(['item_id' => $itemId, 'package_id' => $package->id, 'deleted_at' => 0])->first();
		if ($cartItem) {
			$cartItem->increment('count');

			return;
		}

		$cartItem = new CartItem(['item_id' => $itemId]);
		$package->items()->save($cartItem);
		$package->save();
	}

	public function getOrderData(int $orderId): array
	{
		return $this->preparePackageResultForResponse(
			$this->getPackageItemsQuery()
				->where('packages.order_id', $orderId)
				->get()
		);
	}

	public function updatePackage(Package $package, array $updateData): Package
	{
		foreach ($updateData as $name => $value) {
			$package->$name = $value === 'null' ? null : $value;
		}
		$package->save();

		return $package;
	}

	public function createOrder(User $user, array $packageIds): Order
	{
		$order = Order::create(['user_id' => $user->id]);
		Package::whereIn('id', $packageIds)->update(
			[
				'order_id' => $order->id,
				'user_id' => $order->user_id,
				'status' => Package::STATUS_ORDERED
			]
		);

		return $order;
	}

	private function getPackageItemsQuery()
	{
		return DB::table('items')
			->select('items.*', 'cart_items.id as cart_item_id', 'cart_items.count', 'packages.status', 'packages.shipping', 'packages.payment', 'cart_items.package_id', 'users.name',
				'uploads.path', 'uploads.thumb_filename')
			->join('cart_items', 'cart_items.item_id', '=', 'items.id')
			->join('packages', 'cart_items.package_id', '=', 'packages.id')
			->join('users', 'packages.seller_id', '=', 'users.id')
			->join('uploads', 'cart_items.item_id', '=', 'uploads.item_id')
			->whereNull('cart_items.deleted_at');
	}

	private function preparePackageResultForResponse(Collection $result): array
	{
		$sellers = [];
		foreach ($result as $row) {
			$row->imgSrc = Upload::where('item_id', $row->id)->get()->first()->getUrl(true);
			$sellers[$row->package_id]['id'] = $row->package_id;
			$sellers[$row->package_id]['name'] = $row->name;
			$sellers[$row->package_id]['status'] = $row->status;
			$sellers[$row->package_id]['shipping'] = $row->shipping;
			$sellers[$row->package_id]['payment'] = $row->payment;
			$sellers[$row->package_id]['items'][$row->id] = $row;
		}

		return $sellers;
	}

}
