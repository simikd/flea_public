<?php


namespace App\Services;


use App\Models\Item;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ItemService
{

	public const SLUG_SEPARATOR = '-';

	public function createItem(array $itemData): Item
	{
		if ( isset($itemData['password']) && $itemData['password'] ) {
			$itemData['password'] = Hash::make($itemData['password']);
		}

		$item = Item::create($itemData);
		$item->slug = Str::slug($item->title, self::SLUG_SEPARATOR, 'sk') . self::SLUG_SEPARATOR . $item->id;
		$item->save();

		return $item;
	}

}