<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.1.2019
 * Time: 18:53
 */

namespace App\Services;


use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UserService
{

	private $user;
	private $orderService;
	private $userTrackingService;

	public function __construct(UserTrackingService $userTrackingService, OrderService $orderService)
	{
		$this->userTrackingService = $userTrackingService;
		$this->orderService = $orderService;
	}

	public function initializeUserSession(User $user): void
	{
		$this->user = $user;
		View::share('user', $user);
		View::share('packages', $this->orderService->getUserPackagesData($user->id, false));
	}

	public function checkAndInitializeUnknownUserByIp(String $ip): void
	{
		$user = User::where('ip', $ip)->latest()->first() ?? User::create(['ip' => $ip]);
		$this->initializeUserSession($user);
		Cookie::queue('userId', $user->id);
	}

	private function createUser(array $userData): User
	{
		return User::create($userData);
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function updateUser(array $userData, bool $register): User
	{
		if ($register) {
			return $this->registerUser($userData);
		}

		if ($this->user->email && $userData['email'] && $userData['email'] !== $this->user->email) {
			$userData['ip'] = $this->user->ip;
			$user = User::where(['ip' => $this->user->ip, 'email' => $userData['email']])->first() ?? $this->createUser($userData);
			$this->userTrackingService->reassignUsersSession($this->user, $user);
			$this->user = $user;

			return $user;
		}

		$this->user->update($userData);

		return $this->user;
	}

	public function registerUser($userData): User
	{
		$userData['password'] = Hash::make($userData['password']);
		$userData['registered'] = 1;
		$user = $this->updateUser($userData, false);
		event(new Registered($user));

		return $user;
	}

}
