<?php


namespace App\Events;


use App\Models\Package;
use Illuminate\Queue\SerializesModels;

class PackageUpdatingEvent
{

	use SerializesModels;

	private $package;

	public function __construct(Package $package)
	{
		$this->package = $package;
	}

	public function getPackage(): Package
	{
		return $this->package;
	}

}
