<?php


namespace App\Interfaces;


use Illuminate\Http\Request;

interface CustomValidationInterface
{

	public static function validate(Request $request): array;

}