<?php


namespace App\Interfaces;


use App\Models\Package;

interface PackageUpdaterChecksInterface
{

	public function checkBeforeUpdate(Package $package): bool;

}
