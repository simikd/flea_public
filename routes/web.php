<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomepageController@homepageAction')->name('homepage')->middleware('user.check');
//Route::get('/kategoria/{slug}', 'HomepageController@homepageAction')->middleware('user.check');
//
//// Breadcrumbs
Route::get('/breadcrumbs/categories/{categoryId}', 'BreadcrumbsController@categories');
//
//// Cities
Route::get('/city/list', 'CityController@listCities');
//
//// Orders
Route::view('/vas-kosik', 'order.create', ['checked' => 0])->middleware('user.check');
Route::view('/vytvorenie-objednavky', 'order.create', ['checked' => 1])->middleware('user.check');
//
Route::post('/order/item/add', 'OrderController@addItem')->middleware('user.check');
Route::post('/order/item/remove', 'OrderController@removeItem')->middleware('user.check');
//
Route::get('/order/package/list/checked', 'OrderController@listPackages')->defaults('checked', true)->middleware('user.check');
Route::get('/order/package/list', 'OrderController@listPackages')->defaults('checked', false)->middleware('user.check');
Route::post('/order/package/update', 'OrderController@updatePackage')->middleware('user.check');
Route::post('/order/packages/update', 'OrderController@updatePackages')->middleware('user.check');
//
Route::post('/order/create', 'OrderController@createOrder')->middleware('user.check');
//
//// Docs
Route::post('/docs/photos/upload','DocumentController@uploadPictures')->middleware('user.check');
Route::get('/docs/photos/delete/all','DocumentController@deleteAllPictures')->middleware('user.check');
Route::get('/docs/photos/delete/{filename}','DocumentController@deletePicture')->middleware('user.check');
//
//// Items
Route::get('/novy-inzerat', 'ItemController@showItemForm')->name('new-item-form')->middleware('user.check');
Route::post('/item/create', 'ItemController@createItem')->middleware('user.check');
Route::get('/{slug}', 'ItemController@showItem')->middleware('user.check');
//
//// Markets
Route::get('/market/list', 'MarketController@listMarkets')->middleware('user.check');
//
//// User
Route::post('/user/profile/change', 'UserController@changeUserProfile')->middleware('user.check');
Route::get('/user/profile/get', 'UserController@getUserData')->middleware('user.check');
//
//Auth::routes();
