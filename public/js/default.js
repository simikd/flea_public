$(function () {
	$("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");

	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
	});

	// $('.hiddenToggle').change(function () {
	// 	let target = $('.hidden');
	// 	if ($(this).is(':checked')) {
	// 		target.slideDown();
	// 		target.find('input').each(function () {
	// 			if ($(this).hasClass('required')) {
	// 				$(this).attr('required', 'required');
	// 			}
	// 		});
	//
	// 		return;
	// 	}
	//
	// 	target.slideUp();
	// 	target.removeAttr('required');
	// })

});

function addToCart(itemId) {
	$.post('/order/item/add', {item_id : itemId}, function (data) {
		$('.shopping-cart-wrapper').html(data);
		$('#addToCartModal').modal('show');
	})
		.fail(function(data) {
		});
}
