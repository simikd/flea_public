require('./bootstrap');

import Vue from 'vue';
import VeeValidate from 'vee-validate';

Vue.config.devtools = true;
Vue.config.performance = true;



Vue.use(VeeValidate);

Vue.component('order', require('./components/Order.vue').default);
Vue.component('create-item-form', require('./components/CreateItemForm').default);

const app = new Vue({
	el: '#app',
});

