import * as axios from 'axios';

function upload(formData) {
    const url = `docs/photos/upload`;
    return axios.post(url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })// get data
        .then(x => x.data)
        // add url field
        .then(x => x.map(img => Object.assign({},
            img, { url: `/images/${img.id}` })));
}

export { upload }
