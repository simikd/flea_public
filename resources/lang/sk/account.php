<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'try_free_month' => 'Vyskúšat na mesiac zdarma',
	'login' => 'prihlásenie',
	'register' => 'registrácia',
	'email' => 'email',
	'password' => 'heslo',
	'password_repeat' => 'znova heslo pre overenie',
	'forgotten_password' => 'Zabudli ste heslo?'
];
