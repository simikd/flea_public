<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	@include('layouts.includes.head')
	<body>
		<div id="app" class="container-fluid" style="background-color: #f1f1f1">
			@include('navs.top')
			<div class="container-main">
				@yield('content')
			</div>
		</div>
		@include('layouts.includes.footer')
		<!-- SCRIPTS -->
		<!-- JQuery -->
		<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<!-- MDB core JavaScript -->
		<script type="text/javascript" src="js/mdb.js"></script>
		<script src="/js/default.js"></script>
		@yield('javascripts')
	</body>
</html>
