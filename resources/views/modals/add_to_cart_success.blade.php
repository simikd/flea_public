<div class="modal fade" id="addToCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-notify modal-success modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary">
				<h3 class="heading lead">Inzerát bol pridaný do košíka</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="white-text">&times;</span>
				</button>
			</div>
			<div class="modal-body p-3">
				<h4>Obsah nákupného košíka</h4>
				<div class="text-center">
					<div class="shopping-cart-wrapper col">
						@include('components.includes.shopping_cart_items')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Central Modal Medium Info-->
<div class="bg-canvas bg-new-item"></div>

