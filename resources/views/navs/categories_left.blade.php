<div class="">
	<ul class="" >
		@foreach ($categories as $category)
			<li class="d-block" style="opacity: .8">
				<a href="/kategoria/{{ $category->slug }}" class="category-link py-2 d-block">
					<img width="30px" height="30px" src="/img/icons/cat{{ $category->id }}.png" style="fill: #ffa465 " alt=""/>
					<span class="pl-2 h5 align-bottom black-text">{{ $category->name }}</span>
				</a>
			</li>
		@endforeach
	</ul>
</div>
