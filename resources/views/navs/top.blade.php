<!--Navbar -->
<nav class="mb-3 navbar navbar-top shadow-none navbar-expand-lg row">
		<div class="col-lg-2">
			<div class="title">
            	<a class="navbar-brand text-danger" href="/">Flea</a>
        	</div>
		</div>
		<div class="col-lg-8">
			<div class="row">
				<div class="col-5">
					<ul class="navbar-nav" style="text-align: right">
						<li class="nav-item"><button type="button" class="btn btn-sm btn-discreet btn-outline-amber">Rozšírené hľadanie</button></li>
						<li class="nav-item"><button type="button" class="btn btn-sm btn-discreet">Cena od - do</button></li>
						<li class="nav-item"><button type="button" class="btn btn-sm btn-discreet">Všetky kategórie</button></li>
					</ul>
				</div>
				<div class="col-6" style="padding-top: 8px">
					@include('components.searchbar')
				</div>
				<div class="col-1 text-right">
					@include('components.shopping_cart')
				</div>
			</div>
		</div>
		<div class="col-lg-2">
			@include('components.includes.nav_top_account_corner', ['user' => $user])
		</div>
</nav>
<!--/.Navbar -->
