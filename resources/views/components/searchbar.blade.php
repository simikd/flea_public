<form class="form-inline md-form form-sm active-cyan-2 w-100 my-0">
    <input class="form-control w-75 my-0 pb-1" style="font-size: 1.1rem;" type="text" placeholder="Vyhľadaj, čo potrebuješ" aria-label="Search">
    <i class="fas fa-search text-primary" aria-hidden="true"></i>
</form>
