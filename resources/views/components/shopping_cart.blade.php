@if ($packages)
	<div class="pt-1">
		<button type="button" class="btn dropdown-toggle p-0" style="box-shadow: none" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">
			<img width="30px" height="30px" class="icon-hover" src="/img/icons/shopping-basket.png">
		</button>
		<div class="dropdown-menu dropdown-menu-right p-3 cd-cart" style="box-shadow: 1px 1px 20px 0 rgba(0,0,0,0.3);">
			<div class="col shopping-cart-wrapper">
				@include('components.includes.shopping_cart_items')
			</div>
		</div>
	</div>
@endif
