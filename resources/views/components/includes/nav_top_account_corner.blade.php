<ul class="navbar-nav ml-auto nav-flex-icons">
	@if ($user->registered)
		<li class="nav-item">
			<a class="nav-link waves-effect waves-light text-danger">1
				<i class="fas fa-envelope text-danger"></i>
			</a>
		</li>
		<li class="nav-item avatar dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true"
			   aria-expanded="false">
				<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg" class="rounded-circle z-depth-0" alt="avatar image">
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
				<a class="dropdown-item" href="#">Action</a>
				<a class="dropdown-item" href="#">Another action</a>
				<a class="dropdown-item" href="#">Something else here</a>
			</div>
		</li>
	@else
		<li><a href="" class="text-default mr-3 mt-2" data-toggle="modal" data-target="#modalLRForm">{{ __('account.login') }}</a></li>
		<li><button type="button" class="btn btn-sm btn-primary font-weight-bold">{{ __('account.register') }}</button></li>
		@include('modals.login_register')
	@endif
</ul>
