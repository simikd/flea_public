@php
	$total = 0;
@endphp
@foreach($packages as $package)
	@foreach($package['items'] as $cartItem)
		@php
			$total += $cartItem->price * $cartItem->count;
		@endphp
		<div class="row py-2 pr-2" style="min-width: 500px; border-bottom: 1px dashed #ccc;">
			<div class="col-3 pl-0"><img class="w-100 h-auto" src="{{ $cartItem->imgSrc }}"></div>
			<div class="col-5 d-flex align-items-center" style="font-size: 1.3em">{{ $cartItem->title }}</div>
			<div class="col-1 p-0 d-flex align-items-center" style="font-size: 1.3em; text-align: right;">
{{--				{{ $cartItem->count }}x--}}
			</div>
			<div class="col-3 d-flex align-items-center" style="font-size: 1.3em; text-align: right;">{{ $cartItem->price }} €</div>
		</div>
	@endforeach
@endforeach
<div class="row p-2 bg-primary">
	<div class="col-3"></div>
	<div class="col-6 white-text"><h5 class="font-weight-bold">Spolu:</h5></div>
	<div class="col-3 white-text text-right"><h5 class="font-weight-bold">{{ $total }} €</h5></div>
</div>
<div class="row">
	<div class="col-6"></div>
	<div class="col-6 text-right pr-0">
		@include('components.includes.buttons.order_button')
	</div>

</div>
