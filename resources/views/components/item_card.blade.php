@php
$img = $item->uploads->first();
@endphp
<div class="card-darker p-0 row my-3">
	@if ($img)
	<div class="col-3 p-0">
		<a href="/{{ $item->slug }}">
			<img src="{{ $item->uploads->first()->getUrl() }}" class="w-100" alt="{{ $item->title }}">
		</a>
	</div>
	@endif
	<div class="@if ($img) col-6 @else col-9 @endif  p-3">
		<div class="row">
			<div class="col">
				<h4 class="card-title">
					<strong>
						<a class="text-primary" href="/{{ $item->slug }}">{{ $item->title }}</a>
					</strong>
				</h4>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<span class="h5">{{ $item->seller->name }}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-5">
				<ul class="rating">
					<li><i class="fas fa-star"></i></li>
					<li><i class="fas fa-star"></i></li>
					<li><i class="fas fa-star"></i></li>
					<li><i class="fas fa-star"></i></li>
					<li><i class="far fa-star"></i></li>
				</ul>
			</div>
			<div class="col">
				<span>videné 34x</span>
			</div>
			<div class="col">
				<span>pridané {{ \Carbon\Carbon::parse($item->created_at)->format('d.m.Y') }}</span>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<span class="text-dark">
					{{ str_limit($item->description, 300, $end = '...') }}
				</span>
			</div>
		</div>
	</div>
	<div class="col-3 border-left border-left my-3">
		<div class="row">
			<div class="col">
				<span class="font-weight-bold h4 text-dark">{{ $item->price }} €</span>
			</div>
			<div class="col">
				<div class="float-right">
					<a data-toggle="tooltip" data-placement="top" title="Share">
						<i class="fas fa-share-alt text-primary ml-3" style="font-size: 1.6rem"></i>
					</a>
					<a class="active" data-toggle="tooltip" data-placement="top" title="Added to Wishlist">
						<i class="fas fa-heart text-danger ml-3" style="font-size: 1.6rem"></i>
					</a>
				</div>
			</div>
		</div>
		@if ( $statusMessage = $item->getStatusMessage() )
			<div class="row">
				<div class="col">
					<p class="text-black-50 mt-3">{{ $statusMessage }}</p>
				</div>
			</div>
		@else
			<div class="row">
				<div class="col">
					<p class="text-success mt-2">
						@if($item->shipping_price)
							poštovné: {{ $item->shipping_price }} €
						@else
							<span class="my-4 d-block"> </span>
						@endif
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<button type="button" class="btn btn-outline-default btn-large ml-0 cd-add-to-cart w-100" onclick="addToCart({{ $item->id }})">
						Pridať do košíka
					</button>
				</div>
			</div>
		@endif
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-outline-primary ml-0 cd-add-to-cart w-100">
					<a class="text-primary" href="/{{ $item->slug }}">Detail inzerátu</a>
				</button>
			</div>
		</div>
	</div>
</div>
