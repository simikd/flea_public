@extends('layouts.main')
@section('content')
	<div class="row">
		<div class="col-lg-2 col-md-1 pl-0">
			@include('navs.categories_left')
		</div>
		<div class="col-lg-8 col-md-10 col-sm-12 p-4 content-wrapper">
			<div class="row">
				<div class="col-lg-6">
					<div id="mdb-lightbox-ui"></div>
					<div class="mdb-lightbox no-margin" id="gallery">
						<figure class="mb-3 pics all">
							@if ($item->uploads->count())
								<a href="{{ $item->uploads->first()->getUrl() }}"
								   data-size="{{ $item->uploads->first()->width }}x{{ $item->uploads->first()->height }}">
									<img alt="picture" src="{{ $item->uploads->first()->getUrl() }}"
									     class="z-depth-1 w-100 h-auto">
								</a>
							@endif
						</figure>
					</div>
				</div>
				<div class="col-lg-6">
					<h1>{{ $item->title }}</h1>
					<div class="row">
						<div class="col-lg-2 font-weight-bold">Popis</div>
						<div class="col-lg-10 pl-4">{{ $item->description }}</div>
					</div>
					<div class="row pt-3">
						<div class="col-lg-2 font-weight-bold">Pridané</div>
						<div class="col-lg-10 pl-4">{{ date('d. m. Y', strtotime($item->created_at)) }}</div>
					</div>
					<div class="row py-3">
						<div class="col-lg-2 font-weight-bold">Predajca</div>
						<div class="col-lg-10 pl-4">
							<div class="row">
								<div class="col" style="font-size: 1.5rem">{{ $user->name }}</div>
							</div>
							<div class="row">
								<div class="col">
									<ul class="rating">
										<li>
											<i class="fas fa-star"></i>
										</li>
										<li>
											<i class="fas fa-star"></i>
										</li>
										<li>
											<i class="fas fa-star"></i>
										</li>
										<li>
											<i class="fas fa-star"></i>
										</li>
										<li>
											<i class="far fa-star"></i>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2 font-weight-bold">cena</div>
						<div class="col-lg-5 pl-3" style="font-size: 2.5rem; margin-top: -18px">{{ $item->price }}€
						</div>
					</div>
					<div class="row">
						@if ( $statusMessage )
							<div class="col-lg-6">
								<h5 class="text-primary">{{ $statusMessage }}</h5>
							</div>
						@else
							<div class="col-lg-4">
								<button type="button" class="btn btn-danger btn-large ml-0 cd-add-to-cart"
								        onclick="addToCart({{ $item->id }})">Pridať do košíka
								</button>
							</div>
							<div class="col-lg-4">
								<button type="button" class="btn btn-default btn-large">Objednať ihneď</button>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div>
				<ul class="nav nav-tabs" id="itemTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="item-gallery-tab" data-toggle="tab" href="#item-gallery"
						   aria-selected="true">Galéria</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-primary" id="detail-tab" data-toggle="tab" href="#detail"
						   aria-selected="false">Detail inzerátu</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-primary" id="more-tab" data-toggle="tab" href="#more"
						   aria-selected="false">Ďalšie inzeráty predajcu</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-primary" id="rating-tab" data-toggle="tab" href="#rating"
						   aria-selected="false">Hodnotenie predajcu</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-primary" id="contact-tab" data-toggle="tab" href="#contact"
						   aria-selected="false">Kontaktovať predajcu</a>
					</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="tab-content row" id="myTabContent">
						<div class="tab-pane fade show active col-md-12" id="item-gallery" role="tabpanel"
						     aria-labelledby="item-gallery-tab">
							<div id="mdb-lightbox-ui"></div>
							<div class="mdb-lightbox @if (count($item->uploads) >= 4) gg-box @endif">
								@foreach ($item->uploads as $picture)
									<figure class="gg-element">
										<!--Large image-->
										<a href="{{ $picture->getUrl() }}"
										   data-size="{{ $picture->width }}x{{ $picture->height }}" class="gg-element">
											<!-- Thumbnail-->
											<img src="{{ $picture->getUrl(true) }}"
											     @if (count($item->uploads) < 4) style="padding: 10px; height: 320px; width: auto;"
											     @endif  alt="{{ $item->name }}" class="img-fluid">
										</a>
									</figure>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
							<h2>{{ $item->title}}</h2>
							<p>{{ $item->description }}</p>
						</div>
						<div class="tab-pane fade" id="more" role="tabpanel" aria-labelledby="more-tab">

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-1"></div>
		@include('modals.add_to_cart_success')
	</div>
@endsection
