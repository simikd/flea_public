@extends('layouts.main')
@section('content')
	<div class="row">
		<div class="col-3">
			@include('navs.categories_left')
		</div>
		<div class="col-6">
			@foreach ($items as $item)
				@include('components.item_card', ['item' => $item])
			@endforeach
		</div>
		<div class="col-2">
			<div class="btn-group-vertical btn-group-lg">
				<div class="add w-100">
					<a href="/novy-inzerat" class="btn btn-secondary pl-3 w-100 text-left font-weight-bold"><i class="fa fa-plus pr-3 " style="font-size: 1rem;"></i>Pridať inzerát</a>
				</div>
				<div class="top w-100">
					<a href="/propaguj-inzerat" class="btn btn-primary darken-2 pl-3 w-100 text-left font-weight-bold"><i class="far fa-arrow-alt-circle-up pr-3 " style="font-size: 1rem"></i>Propagovať inzerát</a>
				</div>
				<div class="club w-100">
					<a href="/flea-klub" class="btn btn-danger pl-3 w-100 text-left font-weight-bold"><i class="far fa-star pr-3 " style="font-size: 1rem"></i>Flea Klub</a>
				</div>
			</div>
		</div>
		<div class="col-1"></div>
		<div class="bg-slider"></div>
	</div>
	@include('modals.add_to_cart_success')
@endsection
