@extends('layouts.main')
@section('content')
	<order :checked="{{ $checked ?? false }}"></order>
@endsection

@section('javascripts')
	<script src="/js/app.js"></script>
@stop
